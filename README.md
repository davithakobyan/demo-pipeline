# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### How to build the app using maven ###
Use the maven commands to build the stipulations service app
 
    mvn clean package
        
### How to run spring boot application without servlet container ###
Use the maven commands to start spring boot applications from the terminal    
    
    mvn spring-boot:run

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact