package com.clearlane.pipeline.demo;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class DemoPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPipelineApplication.class, args);
	}

}
