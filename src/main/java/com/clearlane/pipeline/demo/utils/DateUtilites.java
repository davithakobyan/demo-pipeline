package com.clearlane.pipeline.demo.utils;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/11/20 - 8:35 PM
 */
public class DateUtilites {

    private static final Logger log = LoggerFactory.getLogger(DateUtilites.class);

    private static SimpleDateFormat shortFormatter = new SimpleDateFormat("MM/dd/yyyy");

    private static SimpleDateFormat fullFormatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");

    static final int DefaultInterval = 30;

    /**
     * convert date in string format to Date
     * @param dob String
     * @return Date
     */
    public static Date toDate(String dob) {
        if (StringUtils.isEmpty(dob))
            return null;

        try {
            return shortFormatter.parse(dob);
        } catch (ParseException e) {
            log.error("unable to convert string to date {}", dob);
        }
        return null;
    }

    /**
     * Create Date
     * @param month int
     * @param day int
     * @param year int
     * @return Date
     */
    public static Date createDate(int month, int day, int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }

    /**
     * check expiration date, where date range is 30 days
     * @param expirationDate Date
     * @return boolean
     */
    public static boolean checkExpirationDate(Date expirationDate) {
        return checkExpirationDate(expirationDate, DefaultInterval);
    }

    /**
     * check expiration date, where date range is 30 days
     * @param expirationDate Date
     * @param interval int
     * @return boolean
     */
    public static boolean checkExpirationDate(Date expirationDate, int interval) {
        if (expirationDate == null)
            return false;

        Calendar curDate = Calendar.getInstance();
        curDate.add(Calendar.DATE, interval);
        Date time = curDate.getTime();
        return time.after(expirationDate);
    }

    /**
     * Convert Date to String
     * @param date Date
     * @return String
     */
    public static String dateToString(Date date) {
        if (date == null) return null;
        return fullFormatter.format(date);
    }
}
