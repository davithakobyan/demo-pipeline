package com.clearlane.pipeline.demo.utils;

import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Davit Hakobyan
 * @version 1.0
 * @since 3/20/20 - 1:16 PM
 */
class DateUtilitesTest {

    @Test
    void dateToString() {
        String stringDate = DateUtilites.dateToString(null);
        assertNull(stringDate);

        int month = 8, day = 31, year = 1977;
        Date date = DateUtilites.createDate(month, day, year);
        stringDate = DateUtilites.dateToString(date);
        assertNotNull(stringDate);
    }
}